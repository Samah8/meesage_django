from django.test import TestCase
from .models import Post
from django.urls import reverse

class Homepagetest (TestCase):
    def set_up(self):
        Post.objects.create(text="just a test")

     def test_text(self):
         resp=Post.objects.get(id=1)
         expect_text=resp.text
         self.assertEqual(expect_text,"just a test")

    def test_homepage(self):
        response=self.client.get(reverse("home"))
        self.assertEqual(response.status_code,200)    

        

# Create your tests here.
